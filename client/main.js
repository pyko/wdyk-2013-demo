Template["reading-list"].books = function () {
   return Books.find({},{sort: {title: 1}}).fetch();      
};

Template["reading-list"].events({
   "click .remove" : function(){
      Books.remove({_id: this._id});
   }
});