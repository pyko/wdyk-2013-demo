Simple Meteor app used in my "Meteor: A Rising Star" lightning talk at What do you know - Sydney, 2013.

You'll need to install [Meteor][1] to run the app.

Use the ```sample_data.txt``` if you want to add some entries to the database.

Have fun :)

 [1]: http://www.meteor.com